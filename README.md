![Log Icon](icon.png)

# log

`log` is a command-line utility for time-tracking creative output.

## Usage

Log entries are given a **Realm** and **Project**. An entry's **Realm** is a vague categorization, such as "Code", "Visual", or "Audio". An entry's **Project** is the name of the project being worked on. This allows you to track long-term work.

For instance, while programming `log`, I used **Realm** "Code" and **Project** "Log".

### Begin tracking

	log s [Realm] [Project]
	log s Code Log

If a project is already in progress when you begin tracking, it will end and your new project will begin. `log` does not allow multi-tasking.

### End tracking

	log e

### Event logging

	log v [Event]

Events will not be logged unless a project is in progress.

Events are `log`'s mechanism of tracking achievements in addition to time. You can think of them like commit messages.

For example, while programming `log`, I created an event "Log reading support" then later I created another event "Log writing support".

### Other commands

	log c

Print the current in-progress **Realm** and **Project** in format `Realm : Project`. If `log` is not tracking, it will output `_ : _` and return exit code 1.

	log d

Dump the log JSON to stdout.

### log.json format

The log is stored at `~/.log.json` as an array of objects. There are three kinds of objects: start, end, and event.

#### start object

	{
		k: "start",
		t: TIME (UNIX timestamp @ UTC),
		r: REALM,
		p: PROJECT
	}

#### end object

	{
		k: "end",
		t: TIME (UNIX timestamp @ UTC)
	}

#### event object

	{
		k: "event",
		t: TIME (UNIX timestamp @ UTC),
		v: EVENT
	}

Events objects can only appear between start objects and end objects.

## Inspiration

The cadet log is inspired by [Josh Avenier's Log](https://github.com/joshavanier/log), [V-OS' Timekeeping](http://v-os.ca/timekeeping), and [Horaire](http://wiki.xxiivv.com/horaire).

## License

![CC0 Badge](https://licensebuttons.net/p/mark/1.0/88x31.png)  
`log` is licensed under [CC0](https://creativecommons.org/publicdomain/zero/1.0/).
