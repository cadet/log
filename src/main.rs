use std::io;
use std::env;
use std::process;
use std::fs::File;
use std::io::prelude::*;
use std::error::Error;

#[macro_use]
extern crate json;
use json::JsonValue;

extern crate time;

fn usage() {
        eprintln!("Usage:");
        eprintln!("\tlog s [Realm] [Project] : start / switch to project");
        eprintln!("\tlog e : end project");
        eprintln!("\tlog c : print current project");
        eprintln!("\tlog v [Event] : log event");
        eprintln!("\tlog d : dump log contents");
        process::exit(1);
}

fn read_log_file() -> Result<JsonValue, io::Error> {
    let mut log_path = env::home_dir().unwrap();
    log_path.push(".log.json");

    let log = if log_path.exists() {
        let mut file = match File::open(&log_path) {
            Err(why) => panic!("Couldn't open {}: {}", log_path.display(), why.description()),
            Ok(file) => file,
        };
        let mut contents = String::new();
        match file.read_to_string(&mut contents) {
            Err(why) => panic!("Couldn't read {}: {}", log_path.display(), why.description()),
            Ok(_) => ()
        };
        json::parse(&contents)
    } else {
        Ok(JsonValue::new_array().into())
    };

    Ok(log.unwrap().into())
}
fn write_log_file(log: &JsonValue) {
    let mut log_path = env::home_dir().unwrap();
    log_path.push(".log.json");

    let mut file = match File::create(&log_path) {
        Err(why) => panic!("Couldn't create {}: {}", log_path.display(), why.description()),
        Ok(file) => file,
    };

    match log.write(&mut file) {
        Err(why) => panic!("Couldn't write to {}: {}", log_path.display(), why.description()),
        Ok(file) => file,
    };
}

struct Project {
    realm: String,
    project: String
}

enum CurrentProject {
    Project(Project),
    None
}

fn current_project(log: &JsonValue) -> CurrentProject {
    let mut ended = false;
    for entry in log.members().rev() {
        if entry["k"] == "end" { ended = true; }
        if entry["k"] == "start" {
            if !ended {
                return CurrentProject::Project(Project {
                    realm: entry["r"].as_str().unwrap().to_string(),
                    project: entry["p"].as_str().unwrap().to_string()
                });
            } else {
                ended = false;
            }
        }
    }
    CurrentProject::None
}

fn start_project(log: &mut JsonValue, realm: String, project: String) {
    log.push(object!{
        "k" => "start",
        "t" => get_time(),
        "r" => realm,
        "p" => project
    }).expect("Could not create JSON object.");
}

fn end_project(log: &mut JsonValue) {
    log.push(object!{
        "k" => "end",
        "t" => get_time(),
    }).expect("Could not create JSON object.");
}

fn get_time() -> i64 {
    time::now_utc().to_timespec().sec
}

fn main() {
    let args: Vec<_> = env::args().collect();
    if args.len() == 1 {
        usage();
    }
    let mut log = read_log_file().unwrap();

    match args[1].as_ref() {
        "c" => {
            let project = current_project(&log);
            match project {
                CurrentProject::Project(p) => println!("{} : {}", p.realm, p.project),
                CurrentProject::None => {
                    println!("_ : _");
                    process::exit(1);
                }
            }
        },
        "d" => println!("{}", json::stringify_pretty(log, 2)),
        "s" => {
            if args.len() < 4 {
                eprintln!("log s [Realm] [Project]");
                process::exit(1);
            }
            match current_project(&log) {
                CurrentProject::Project(_) => end_project(&mut log),
                CurrentProject::None => (),
            };
            start_project(&mut log, env::args().nth(2).unwrap(), env::args().nth(3).unwrap());
            write_log_file(&log);
        },
        "e" => {
            match current_project(&log) {
                CurrentProject::Project(_) => {
                    end_project(&mut log);
                    write_log_file(&log);
                },
                CurrentProject::None => (),
            };
        },
        "v" => {
            if args.len() < 2 {
                eprintln!("log v [Event]");
                process::exit(1);
            }
            match current_project(&log) {
                CurrentProject::Project(_) => {
                    log.push(object!{
                        "k" => "event",
                        "t" => get_time(),
                        "v" => env::args().nth(2).unwrap()
                    }).expect("Could not create JSON object.");
                    write_log_file(&log);
                },
                CurrentProject::None => (),
            };
        },
        _ => usage(),
    }
}
